package com.calebpower.mc.mcsudo;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * <strong>MC Sudo</strong> allows a player to execute a command as wither the
 * console or as a player that is currently online.
 * 
 * @author LordInateur
 */
public class Core extends JavaPlugin {

  /**
   * When the plugin is enabled, add a message to the log indicating as such.
   */
  @Override public void onEnable() {
    Messenger.log("Created by LordInateur.");
  }
  
  /**
   * When the plugin is disabled, add a message to the log indicating as such.
   */
  @Override public void onDisable() {
    Messenger.log("Disabling plugin...");
  }
  
  /**
   * When a command pertaining to this plugin is executed, act accordingly.
   */
  @Override public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
    Messenger.sendMessage(sender, "You have just executed the *sudo* command.");
    return true;
  }
  
}
